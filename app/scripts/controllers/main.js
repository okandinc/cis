'use strict';
var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');

/**
 * @ngdoc function
 * @name cisApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the cisApp
 */
angular.module('cisApp')
  .controller('MainCtrl', function ($scope, $http) {
    $scope.selectedCampus = 'ayazaga';
    $scope.selectedBaseLayer = 'osm';
    $scope.selectedPoiTypeId = '0';

    var mapView = new ol.View({
      projection: "EPSG:4326",
      center: [29.027133, 41.104864],
      zoom: 15
    });
    //BaseLayers
    var osm = new ol.layer.Tile({
      source: new ol.source.OSM({
        name: 'Open Street Map'
      }),
      visible: true,
      zIndex: 0
    });
    var bingRoad = new ol.layer.Tile({
      source: new ol.source.BingMaps({
        key: 'AqyQ7cBaSfO_bnLz5ANtmRr-Q1jmOim2Ng1m1EShZ0AQmOi8tz5n-CwKgeIGxvFw',
        name: 'Bing Sokak',
        imagerySet: 'Road'
      }),
      visible: false,
      zIndex: 0
    });
    var bingAerial = new ol.layer.Tile({
      source: new ol.source.BingMaps({
        key: 'AqyQ7cBaSfO_bnLz5ANtmRr-Q1jmOim2Ng1m1EShZ0AQmOi8tz5n-CwKgeIGxvFw',
        name: 'Bing Uydu',
        imagerySet: 'AerialWithLabels'
      }),
      visible: false,
      zIndex: 0
    });
    var mapBoxTerrain = new ol.layer.Tile({
      source: new ol.source.XYZ({
        name: 'İTU',
        url: 'http://api.tiles.mapbox.com/v4/okandinc.cf800e44/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoib2thbmRpbmMiLCJhIjoibUhxUjEtOCJ9.A3zULArhNfQvBU2mgvNKcA',
        type: 'xyz'
      }),
      visible: false,
      zIndex: 0
    });
    var googleSat = new ol.layer.Tile({
      source: new ol.source.XYZ({
        name: 'Google Satellite',
        url: 'https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',
        type: 'xyz',
        projection: 'EPSG:3857',
        tileGrid: ol.tilegrid.createXYZ({
          extent: [-20037508.342789244, -20037508.342789244, 20037508.342789244, 20037508.342789244]})
      }),
      visible: false,
      zIndex: 0
    });
    var googleStreet = new ol.layer.Tile({
      source: new ol.source.XYZ({
        name: 'Google Streets',
        url: 'https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
        type: 'xyz',
        projection: 'EPSG:3857',
        tileGrid: ol.tilegrid.createXYZ({
          extent: [-20037508.342789244, -20037508.342789244, 20037508.342789244, 20037508.342789244]})
      }),
      visible: false,
      zIndex: 0
    });
    //WFSLayer
    var vectorSource = new ol.source.Vector({
      format: new ol.format.GeoJSON(),
      url: function (extent) {
        return 'http://localhost:8085/geoserver/tez/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=tez:poi&outputFormat=application%2Fjson'
      },
      strategy: ol.loadingstrategy.all
    });
    var wfsLayerStyleFunc = function (feature) {
      var opacity = 0;
      var style = new ol.style.Style();

      if ($scope.selectedPoiTypeId == 0 || feature.get('tur_id') == $scope.selectedPoiTypeId) {
        opacity = 1;
      }
      style.setImage(new ol.style.Icon({
        anchor: [0.5, 1.0],
        snapToPixel: false,
        opacity: opacity,
        src: feature.get('icon_path'),
        scale: 0.5
      }));

      return style;
    };
    var poiVectorLayer = new ol.layer.Vector({
      source: vectorSource,
      zIndex: 5,
      visible: true,
      style: wfsLayerStyleFunc
    });

    var overlay = new ol.Overlay({
      element: container,
      autoPan: true,
      autoPanAnimation: {
        duration: 250
      }
    });

    var map = new ol.Map({
      controls: ol.control.defaults({
        attributionOptions: {
          collapsible: false
        }
      }),
      target: 'map',
      layers: [osm, bingRoad, bingAerial, mapBoxTerrain, googleSat, googleStreet, poiVectorLayer,
        new ol.layer.Tile({
          source: new ol.source.TileWMS({
            url: 'http://localhost:8085/geoserver/wms',
            params: {'LAYERS': 'tez:kampus_alani'},
            serverType: 'geoserver'
          }),
          visible: true,
          zIndex: 4
        })
      ],
      overlays: [overlay],
      view: mapView
    });

    var onMapFirstRendered = function () {
    };

    closer.onclick = function() {
      overlay.setPosition(undefined);
      closer.blur();
      return false;
    };


    map.on('singleclick', function (evt) {
      map.forEachFeatureAtPixel(evt.pixel,
        function (feature, layer) {
          var coordinate = evt.coordinate;
          var string = feature.get('adi');
          content.innerHTML = string;
          overlay.setPosition(coordinate);
        });
    });

    map.once('postrender', onMapFirstRendered);

    $scope.changeBaseLayer = function () {
      if ($scope.selectedBaseLayer === 'bingRoad') {
        bingRoad.setVisible(true);
        osm.setVisible(false);
        bingAerial.setVisible(false);
        mapBoxTerrain.setVisible(false);
        googleSat.setVisible(true);
        googleStreet.setVisible(false)
      }
      if ($scope.selectedBaseLayer === 'osm') {
        bingRoad.setVisible(false);
        osm.setVisible(true);
        bingAerial.setVisible(false);
        mapBoxTerrain.setVisible(false);
        googleSat.setVisible(true);
        googleStreet.setVisible(false)
      }
      if ($scope.selectedBaseLayer === 'bingAerial') {
        bingRoad.setVisible(false);
        osm.setVisible(false);
        bingAerial.setVisible(true);
        mapBoxTerrain.setVisible(false);
        googleSat.setVisible(false);
        googleStreet.setVisible(false)
      }
      if ($scope.selectedBaseLayer === 'mapbox_terrain') {
        bingRoad.setVisible(false);
        osm.setVisible(false);
        bingAerial.setVisible(false);
        mapBoxTerrain.setVisible(true);
        googleSat.setVisible(false);
        googleStreet.setVisible(false)
      }
      if ($scope.selectedBaseLayer === 'googleSat') {
        bingRoad.setVisible(false);
        osm.setVisible(false);
        bingAerial.setVisible(false);
        mapBoxTerrain.setVisible(false);
        googleSat.setVisible(true);
        googleStreet.setVisible(false)
      }
      if ($scope.selectedBaseLayer === 'googleStreet') {
        bingRoad.setVisible(false);
        osm.setVisible(false);
        bingAerial.setVisible(false);
        mapBoxTerrain.setVisible(false);
        googleSat.setVisible(false);
        googleStreet.setVisible(true)
      }

    };

    $scope.changeCampus = function () {

      if ($scope.selectedCampus === 'gumussuyu') {
        map.getView().setCenter([28.991385, 41.038184]);
        map.getView().setZoom(17);
      }

      if ($scope.selectedCampus === 'macka') {
        map.getView().setCenter([28.995762, 41.045289]);
        map.getView().setZoom(17);
      }

      if ($scope.selectedCampus === 'taskisla') {
        map.getView().setCenter([28.990677, 41.041178]);
        map.getView().setZoom(18);
      }
      if ($scope.selectedCampus === 'tuzla') {
        map.getView().setCenter([29.294526, 40.812968]);
        map.getView().setZoom(17);
      }

      if ($scope.selectedCampus === 'ayazaga') {
        map.getView().setCenter([29.027133, 41.104864]);
        map.getView().setZoom(15);
      }
    };

    $scope.changePoiType = function () {
      angular.forEach(map.getLayers(), function (layer) {
        layer.getSource().changed();
      });
    }
  });
