'use strict';

/**
 * @ngdoc function
 * @name cisApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the cisApp
 */
angular.module('cisApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
